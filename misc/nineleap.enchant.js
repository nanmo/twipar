/* 
 * enchant.js plugin for 9leap.net (v2.0)
 * 2011/05/28 supported twitter api
 */

enchant.nineleap = { assets: ['start.png', 'end.png'] };
enchant.nineleap.Game = enchant.Class.create(enchant.Game, {
    initialize: function(width, height) {
        enchant.Game.call(this, width, height);
        var game = this;
        this.addEventListener('load', function() {
            this.startScene = new SplashScene();
            this.startScene.image = game.assets['start.png'];
            this.startScene.addEventListener('touchend', function() {
                if (game.currentScene == this) game.popScene();
            });
            this.pushScene(this.startScene);

            this.endScene = new SplashScene();
            this.endScene.image = game.assets['end.png'];
        });
        
        this.api = {
            queue: 0,
            request: function(path, req, succ){
                this.queue ++;
                var urlprefix = "http://9leap.net/api";
        //        var urlprefix = "http://localhost:8001/api";
                var callback = "callback_" + +new Date() + this.queue;
                var query = "?callback=" + callback;
        
                for(var key in req){
                    query += "&" + key + "=" + req[key];
                }
                
                callback_func = function(obj){
                   if(obj.error){
                       game.api.queue --;
                       if(obj.code == 401){
                           window.location.replace('http://9leap.net/api/login?after_login='+window.location.href);
                       }
                   }else{
                        game.api.queue --;
                        succ(obj);
                        game.start();
                   }
                }
                
                eval(callback + " = callback_func;");
                var jsonp = document.createElement('script');
                jsonp.setAttribute('type', "text/javascript" );
                jsonp.setAttribute('src', urlprefix + path +query );
                document.body.appendChild(jsonp);
            },
            load: function(mode){
                if(mode == "player"){
                    game.api.request("/twitter/account/verify_credentials.json", [],
                    function(obj){
                       game.api.player = obj;
                    });
                }else if(mode == "friends" || mode == "followers"){
                    game.api.request("/twitter/statuses/" + mode+ ".json", [],
                    function(obj){
                       game.api[mode] = obj;
                       for(var i in game.api.friends){
                           game.preload(game.api[mode][i].profile_image_url)
                       }
                    });
                }
            }
        }

    },
    end: function(score, result) {
        this.pushScene(this.endScene);
        if (location.hostname == 'r.jsgames.jp') {
            var submit = function() {
                var id = location.pathname.match(/^\/games\/(\d+)/)[1]; 
                location.replace([
                    'http://9leap.net/games/', id, '/result',
                    '?score=', encodeURIComponent(score),
                    '&result=', encodeURIComponent(result)
                ].join(''));
            }
            this.endScene.addEventListener('touchend', submit);
            window.setTimeout(submit, 3000);
        }
    },
    start: function() {
        console.log("try to start",this.api.queue)
        if (this.api.queue == 0){
            if (this._intervalID) {
                window.clearInterval(this._intervalID);
            } else if (this._assets.length) {
                var o = {};
                var assets = this._assets.filter(function(i) {
                    return i in o ? false : o[i] = true;
                });
                var loaded = 0;
                var total = assets.length;
                while (assets.length) {
                    var src = assets.shift();
                    var asset = enchant.Surface.load(src);
                    asset.onload = function() {
                        var e = new enchant.Event('progress');
                        e.loaded = ++loaded;
                        e.total = total;
                        game.dispatchEvent(e);
                        if (loaded == total) {
                            game.removeScene(game.loadingScene);
                            game.dispatchEvent(new enchant.Event('load'));
                        }
                    };
                    this.assets[src] = asset;
                }
                this.pushScene(this.loadingScene);
            } else {
                this.dispatchEvent(new enchant.Event('load'));
            }
            this.currentTime = Date.now();
            this._intervalID = window.setInterval(function() {
                game._tick()
            }, 1000 / this.fps);
            this.running = true;
        }
    }
    
});

enchant.nineleap.SplashScene = enchant.Class.create(enchant.Scene, {
    image: {
        get: function() {
            return this._image;
        },
        set: function(image) {
            this._image = image;

            while (this.firstChild) {
                this.removeChild(this.firstChild);
            }
            var sprite = new Sprite(image.width, image.height);
            sprite.image = image;
            sprite.x = (this.width - image.width) / 2;
            sprite.y = (this.height - image.height) / 2;
            this.addChild(sprite);
        }
    }
});
